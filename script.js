let result=document.getElementById('result');
let dot=document.getElementById('dot');
let randomBtnClicked=document.getElementById('rndBtn');
let timeOutResult;
//The dot can be clicked once in a calculation
dot.addEventListener('click',function(){
  dot.disabled=true;
});
function addToDisplay(number){
  result.value+=number;
}
function addAction(action){
  //making sure a calculation doesn't start with an action
  if(result.value===""){
    result.value="";
  }else{
    result.value+=action;
    dot.disabled=false;
  }
}
//in case the '=' sign is clicked before entering a number
//also checks for division by zero
function displayResult(){
  if(isFinite(eval(result.value))){
    result.value=eval(result.value).toFixed(3);
  }
  else{
    result.value="You can't divide by 0";
    timeOutResult=setTimeout(function(){
      randomBtnClicked.addEventListener('click',clearResultField())
    },4000);
  }
  if(result.value===""){
    result.value="";
  }else{
    result.value=eval(result.value);
  }
  dot.disabled=false;
}
function clearResultField(){
  result.value="";
  dot.disabled=false;
}
function fullWipe(){
  clearResultField();
  clearTimeout(timeOutResult);
}
